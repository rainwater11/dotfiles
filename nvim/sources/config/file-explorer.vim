" Use vimfiler instead of netrw
let g:vimfiler_as_default_explorer = 1

" Easy access
nnoremap <leader>fe :VimFiler -toggle<CR>

let g:vimfiler_safe_mode_by_default = 0

" Mappings
autocmd FileType vimfiler call s:vimfiler_config()
function s:vimfiler_config()
  " Unmap space
  nunmap <buffer> <Space>
  nmap <buffer> <Leader> <Plug>(vimfiler_edit_file)
endfunction
