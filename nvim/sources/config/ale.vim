let g:ale_fix_on_save = 1

let g:ale_linters = {
\   'javascript': ['eslint'],
\}

let g:ale_fixers = {
\   'javascript': ['prettier', 'eslint']
\ }

