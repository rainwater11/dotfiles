" UltisSnips config
let g:UltiSnipsExpandTrigger = "<c-n>"
let g:UltiSnipsListSnippets  = "<c-tab>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"

" let g:UltiSnipsExpandTrigger = '<tab>'
" let g:UltiSnipsJumpForwardTrigger = '<tab>'
" let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

" g:UltiSnipsSnippetsDir doesn't seem to work anymore
" $HOME must be used instead of ~/
" otherwise it won't work
" To add more directories, simply add a comma before the closing bracket
let g:UltiSnipsSnippetDirectories=[$HOME.'/dotfiles/nvim/snips']
