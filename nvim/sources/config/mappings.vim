" Make Ctrl+o and Ctrl+i work with relative line number movement.
nnoremap <silent> k :<C-U>execute 'normal!' (v:count > 1 ? "m'" . v:count : '') . 'k'<CR>
nnoremap <silent> j :<C-U>execute 'normal!' (v:count > 1 ? "m'" . v:count : '') . 'j'<CR>

" Splits
nnoremap <leader>\| <c-w>v
nnoremap <leader>- <c-w>s
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" <Leader> f for folding/unfolding
nnoremap <Leader>f za

" Y to y$
nnoremap Y y$

" "*yy is for yanking to the clipboard
nnoremap <Leader>yy "*yy

" Ctrl+P to :FZF
nnoremap <c-p> :FZF<cr>

" Silver searcher
nnoremap <leader>ss :Ack 

" Ctrl+S to :w
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

" Ctrl+c to :q
nnoremap <c-c> :q<CR>
inoremap <c-c> <Esc>:q<CR>a

" enter to place a blank line
nnoremap <CR> o<Esc>

" Shift+Enter to Escape+o
"inoremap ^[0M <Esc>o

inoremap <C-CR> <Esc>o

" Snippets
nnoremap <Leader>sn :UltiSnipsEdit<CR>

" Camel case motion
call camelcasemotion#CreateMotionMappings('<leader>')
