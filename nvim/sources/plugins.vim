" File explorer
Plug 'Shougo/unite.vim'
Plug 'Shougo/vimfiler.vim'

" Fuzzy finder
Plug 'junegunn/fzf', {'dir': '~/.fzf', 'do': './install --all'}

" Silver Searcher
Plug 'mileszs/ack.vim'

" Auto set/unset paste
Plug 'roxma/vim-paste-easy'

" Camel case motion
Plug 'bkad/CamelCaseMotion'

" Indent Object
Plug 'michaeljsmith/vim-indent-object'

" Lint
Plug 'w0rp/ale'

" Perform vim insert mode completions with Tab
Plug 'ervandew/supertab'

" Automatic closing of quotes, parenthesis, brackets, etc.
Plug 'Raimondi/delimitMate'

" Comment out a line easily
Plug 'tpope/vim-commentary'

" Provide mappings to easily delete, change and add surroundings 
" (parentheses, brackets, quotes, XML tags, and more) in pairs.
Plug 'tpope/vim-surround'

" Short normal mode aliases for commonly used ex commands
Plug 'tpope/vim-unimpaired'

" Repeat tpope plugin changes
Plug 'tpope/vim-repeat'

" UltiSnips
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

"" Syntax highlighting/indention
" JS
Plug 'pangloss/vim-javascript', {'for': 'javascript'}

" Syntax highlighting and indenting for JSX
Plug 'mxw/vim-jsx', {'for': 'javascript'}

" Docker
Plug 'ekalinin/Dockerfile.vim', {'for': 'Dockerfile'}

" YAML
Plug 'stephpy/vim-yaml', {'for': 'yaml'}

"" Python
" Syntax highlighting for Django templates
Plug 'vim-scripts/django.vim', {'for': 'python'}

" Better code folding
Plug 'tmhedberg/SimpylFold', {'for': 'python'}

"" Lilypond
Plug 'gisraptor/vim-lilypond-integrator', {'for': 'lilypond'}

"" Change order of delmited items such as function calls, method arguments, etc.
Plug 'machakann/vim-swap'

"" WakaTime
Plug 'wakatime/vim-wakatime'
