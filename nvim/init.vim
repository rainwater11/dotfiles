set nocompatible

" Background
set background=dark

" Plugins
call plug#begin('~/.local/share/nvim/plugged')
source ~/.config/nvim/sources/plugins.vim
call plug#end()

" <Leader> to space
let mapleader = " "

" Source all vim files in sources directory
for f in split(glob('~/.config/nvim/sources/config/*.vim'), '\n')
    exe 'source' f
endfor

" Persistent undo
set undofile
set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000

" Line numbers
set number
set relativenumber

" Make Incrementing and decrementing numbers only work with decimal.
set nrformats=

" Enable folding
set foldmethod=indent
set foldlevel=99

" Setting filetype at top of file
set modeline

" ctags
set tags=tags

" show the matching part of the pair for [] {} and ()
set showmatch
set encoding=utf-8

" Disable visual mode via mouse (Can't paste on Ubuntu on Windows if enabled).
set mouse-=a

" Searching
set incsearch

" Splitting windows
set splitbelow
set splitright

" Disable auto comment insertion
au FileType * set fo-=c fo-=r fo-=o
