# vim: filetype=zsh

# Make delete key work on ConEmu
export TERM=xterm

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH=~/.oh-my-zsh

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git vi-mode zsh-nvm)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

PROMPT='%{$fg[cyan]%}%~%{$reset_color%} '

# Makes ctrl-s work in vim
stty stop undef
stty start undef

KEYTIMEOUT=1

# Path
PATH=$PATH:~/bin

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Sources
. ~/.zsh_aliases
. ~/.zsh_functions

# UltiSnips
psn=~/.vim/my-snippets/UltiSnips/python.snippets
jsn=~/.vim/my-snippets/UltiSnips/java.snippets
mysn=~/.vim/my-snippets/UltiSnips
vsn=~/.vim/bundle/vim-snippets/snippets
export PATH


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Node Version Manager
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm

export PATH=$PATH:$HOME/.npm_modules/bin

# Add Lilypond to PATH
[[ ":${PATH}:" == *":${HOME}/.lyp/bin:"* ]] || PATH="$HOME/.lyp/bin:$PATH"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Globbing
setopt extended_glob
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Set Mac OS colors for `ls`
unset LSCOLORS
export CLICOLOR=1
export CLICOLOR_FORCE=1

# Set path to include /usr/local/bin
export PATH=$PATH:/usr/local/git/bin:/usr/local/bin



## Mac/Hackintosh specific ##
# If running mac (not running Linux),
# set sleep to actually sleep instead of looping into sleep/wake
if [ "$(uname 2> /dev/null)" != "Linux" ]; then
  sudo pmset -a hibernatemode 0
fi
